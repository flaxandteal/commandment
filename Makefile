run-db:
	docker-compose up -d db
run:
	docker-compose up -d commandment-api
	sleep 3
	docker-compose up -d commandment-worker redis commandment nginx
stop:
	docker-compose stop
