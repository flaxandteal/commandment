FROM ubuntu:14.04
ENV DEBIAN_FRONTEND noninteractive

# Credit for Dockerfile components from:
# https://github.com/danriti/gunicorn-flask

RUN apt-get update
RUN apt-get install -y python python-pip python-virtualenv gunicorn

# Setup flask application
RUN mkdir -p /deploy/app
COPY gunicorn_config.py /deploy/gunicorn_config.py

EXPOSE 8080

RUN apt-get update -y
RUN apt-get install -y python-pip python-dev build-essential git libssl-dev libffi-dev swig python-psycopg2
RUN pip install --upgrade pip
RUN pip install \
  M2Crypto>=0.25 \
  pyOpenSSL \
  Flask \
  SQLAlchemy>=1.1.0b1 \
  apns \
  oauthlib \
  passlib \
  biplist \
  flask-restful \
  pytest-flask \
  flask-redis \
  fakeredis \
  rq \
  six>=1.7 \
  mock

RUN mkdir -p /deploy
COPY . /deploy/app

WORKDIR /deploy/app
RUN pip install -r requirements.txt
RUN python setup.py install

CMD ["/usr/bin/gunicorn", "--config", "/deploy/gunicorn_config.py", "commandment.wsgi:app"]
