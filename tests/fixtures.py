"""test_enrolment.py: Tests for enrolment behaviour"""

__author__ = "Phil Weir <phil.weir@flaxandteal.co.uk>"

import pytest
import uuid
import datetime
import mock

from fakeredis import FakeStrictRedis
from flask_redis import FlaskRedis

from commandment import api_app as capp, database
from commandment import app as mapp, api as capi
from commandment.models import ProfileStatus, Profile


class MockDictData:
    data = 'Mock_Token_Data'

@pytest.yield_fixture(scope="session")
def app():
    mock_redis = FlaskRedis.from_custom_provider(FakeStrictRedis)
    flask_app = capp.create_api_app(True, mock_redis, {})
    capi.push_to_device = mock.MagicMock()
    database.config_engine('sqlite://', echo=True)
    database.init_db()
    connection = database.engine.connect()

    yield flask_app

    connection.close()
    database.Base.metadata.drop_all(bind=database.engine)


@pytest.yield_fixture(scope="session")
def mdm_app():
    mock_redis = FlaskRedis.from_custom_provider(FakeStrictRedis)
    database.config_engine('sqlite://', echo=True)
    flask_mdm_app = mapp.create_app(True, mock_redis, {})
    app_request = flask_mdm_app.test_request_context()
    app_request.push()
    database.init_db()
    connection = database.engine.connect()
    flask_mdm_app.testing = True

    yield flask_mdm_app

    connection.close()
    database.Base.metadata.drop_all(bind=database.engine)

@pytest.fixture
def profile():
    profile_uuid = str(uuid.uuid4())

    profile = {
        'identifier': 'com.example.test.' + profile_uuid,
        'uuid': profile_uuid,
        'profile_data': '',
        'status': ProfileStatus.INACTIVE
    }

    return profile

@pytest.fixture
def certificate():
    cert = {
        'fingerprint': '0000',
        'subject': 'SUBJECT',
        'cert_type': 'mdm.device',
        'not_before': datetime.datetime(year=1970, month=1, day=1),
        'not_after': datetime.datetime(year=2170, month=1, day=1),
        'pem_certificate': 'CERTIFICATE'
    }

    return cert

@pytest.fixture
def device():
    device_uuid = str(uuid.uuid4())

    device = {
        'udid': device_uuid,
        'serial_number': '90210'
    }

    return device

@pytest.fixture
def mdm_group():
    group_uuid = str(uuid.uuid4())

    group = {
        'group_name': 'Group test ' + group_uuid,
        'description': 'Test group',
    }

    return group


@pytest.fixture
def mdm_profile():
    prefix = str(uuid.uuid4())[:5]
    mdm_profile = {
        'prefix': prefix,
        'topic': 'TestTopic',
        'access_rights': 'MDM_AR__ALL',
        'mdm_url': 'test/url',
        'checkin_url': 'test/checkin_url',
        'mdm_name': 'testName',
        'push_cert_id': 1,
        'device_identity_method': 'provide',
    }
    return mdm_profile


@pytest.fixture
def auth_plist_data():
    plist_data = {
        'MessageType': 'Authenticate',
        'UDID': 'F6631968-54D7-45BD-8744-704CFC1D0C40'
    }
    return plist_data


@pytest.fixture
def token_plist_data():
    plist_data = {
        'MessageType': 'TokenUpdate',
        'UDID': 'F6631968-54D7-45BD-8744-704CFC1D0C45',
        'Token': MockDictData(),
        'Topic': 'Device Topic',
    }
    return plist_data

@pytest.fixture
def exist_device():

    device = {
        'udid': 'F6631968-54D7-45BD-8744-704CFC1D0C45',
        'serial_number': '90210'
    }

    return device

@pytest.fixture
def mdm_plist_data():
    plist_data = {
        'UDID': 'F6631968-54D7-45BD-8744-704CFC1D0C45',
        'Status': 'Acknowledged',
        'CommandUUID': '8A2324B9-AABB-46E4-9E85-3AA7A189FA9B',
        'QueryResponses': {
            'SerialNumber': '1188902',
        }
    }
    return plist_data

@pytest.fixture
def queued_command():
    command = {
        'command_class': 'UpdateInventoryDevInfoCommand',
        'uuid': '8A2324B9-AABB-46E4-9E85-3AA7A189FA9B',
    }
    return command

@pytest.fixture
def certificate_fixture():
    cert_data = {
        'pem_certificate': '-----BEGIN CERTIFICATE-----MIIDEDCCAfigAwIBAgIGf9pcUJdQMA0GCSqGSIb3DQEBCwUAMBExDzANBgNVBAMMBk1ETSBDQTAeFw0xNjEwMTAxMjMzMjNaFw0xNzEwMTAxMjMzMjNaMBUxEzARBgNVBAMMCk1ETSBEZXZpY2UwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQC0ddKfeustggHkaogBMNsaKCbGpufMrKqgt047AARJzgzSFRUoiMTHwJCX7HUt4RCDvY9InNs2tC/WgRTnEDBNGEDi0F0ADXsccDTEToWIutrHFh0hu8yjx62P+f5g+4LkCGTgFDjXDvjYpzYXAnbnYva8dcFlxuxx/MbGwmN7pCBBPafUQt+gVmR6WChHailq14V/UOKJFgfFEyYgaCg004VkS8LiDhHUkiie8nrVXqutd1IwSKC0DItye01aKjQRlill63FFUbX1cmrVGdcwwvOgmj6d9ic76SWltST/msnoD2LW895to++ZH5fEjPFxVUupFpj7JoIe7B9xSPyDAgMBAAGjajBoMB0GA1UdDgQWBBRkJhPa/DuQhx4lw8pgX4hetdAG6DA5BgNVHSMEMjAwgBStEqw40pRO9/ZXTl085P563AfmLqEVpBMwETEPMA0GA1UEAwwGTURNIENBggEBMAwGA1UdEwEB/wQCMAAwDQYJKoZIhvcNAQELBQADggEBAEAyHo1bguawrN5tsKC7WydDu+cjV2qv1FvvWaQsaDowbzYzswchE0JTUOcq4niZYgBekA27Y2O0sitoMWioBUUne2stQAA5qrtcemUkpBeMszrFFzmvWcUc9r30Ot7n8Z12QVXl06+eZqwjPKGuQzKcpJ+48ZpzHpKUkknzrSo08IrKDdc/k+gzK+fxX1W7qsvYhKi3p6n4aipDdFJfl7bR1lFVHPxyff5thwUs7a4K7GuQY+wS8f61g6Q6o5IjTOhyb+p6nIiCRZF8ZIDmj9KwV+AvDzW3g8QsWjeAAypy86xWlu70yWCKaaqgcm6Ut8CQCM0/TQNEq5DXWXPPf0U=-----END CERTIFICATE-----',
        'not_before': datetime.datetime.now(),
        'not_after': datetime.datetime.now(),
        'cert_type': 'mdm.device',
        'fingerprint': 'A7421C04C88EB4932DFD1277DA1B401472E91E95457D7493FE2ED5E95BFB5B92',
    }
    return cert_data
