import plistlib
import pytest
from fixtures import mdm_app, exist_device, mdm_plist_data, queued_command
from mock import MagicMock, Mock
from flask import url_for, g
from commandment.mdm import device
from commandment.mdm import actions
from commandment import database as cdatabase
from commandment.models import Certificate, Device, QueuedCommand


class TestMDM:
    def test_do_mdm_with_idle_status(self, mdm_app, exist_device, mdm_plist_data, queued_command):
        device_inst = Device(**exist_device)
        cdatabase.db_session.add(device_inst)
        cdatabase.db_session.commit()

        q_command = QueuedCommand(**queued_command)
        q_command.device = device_inst
        cdatabase.db_session.add(q_command)
        cdatabase.db_session.commit()

        mdm_plist_data['Status'] = 'Idle'
        g.device = device_inst
        g.plist_data = mdm_plist_data

        mdm = actions.do_mdm()
        assert mdm.status_code == 200
        response_plist = plistlib.readPlistFromString(mdm.data)
        assert response_plist['CommandUUID'] == queued_command['uuid']


    def test_do_mdm_with_acknowledged_status(self, mdm_app, exist_device, mdm_plist_data, queued_command):
        device_inst = Device(**exist_device)
        cdatabase.db_session.add(device_inst)
        cdatabase.db_session.commit()

        queued_command['uuid'] = '8A2324B9-AABB-46E4-9E85-3AA7A189FA99'
        q_command = QueuedCommand(**queued_command)
        q_command.device = device_inst
        cdatabase.db_session.add(q_command)
        cdatabase.db_session.commit()

        g.device = device_inst
        mdm_plist_data['CommandUUID'] = '8A2324B9-AABB-46E4-9E85-3AA7A189FA99'
        g.plist_data = mdm_plist_data
        mdm = actions.do_mdm()
        assert mdm == ''

        command = QueuedCommand.find_by_uuid(mdm_plist_data['CommandUUID'])
        assert command.result == mdm_plist_data['Status']

        updated_device = updated_device = cdatabase.db_session.query(Device).filter(Device.id == device_inst.id).one()
        assert updated_device.serial_number == mdm_plist_data['QueryResponses'].get('SerialNumber')

