"""test_enrolment.py: Tests for enrolment behaviour"""

__author__ = "Phil Weir <phil.weir@flaxandteal.co.uk>"

import json
import uuid
import pytest

from mock import MagicMock
from flask import url_for

from fixtures import mdm_app, app, mdm_profile
from commandment import database as cdatabase
from commandment.mdm import enroll, actions
from commandment.models import Device, MDMConfig



class TestEnrolment:
    def test_enroll_from_plist_finds_existing_device(self, app):
        plist = {
            'UDID': '9b99e22c-c6b3-4768-8ab5-76c29f4021ac',
            'SERIAL': '7b2c5e8e-4457-4a34-a942-05d5973c619e'
        }
        device = Device()
        device.serial_number = plist['SERIAL']
        device.udid = plist['UDID']
        cdatabase.db_session.add(device)
        cdatabase.db_session.commit()

        redis_client = app.redis_store._redis_client

        pubsub = {}
        for channel in ('enroll', 'serial'):
            pubsub_channel = redis_client.pubsub()
            pubsub_channel.subscribe('commandment.' + channel)
            pubsub_channel.get_message()
            pubsub[channel] = pubsub_channel

        token = str(uuid.uuid4())
        new_device = enroll.enroll_from_plist(plist, token)

        message = pubsub['enroll'].get_message()['data']
        assert pubsub['enroll'].get_message() is None

        payload = json.loads(message)
        assert payload == {'udid': plist['UDID'], 'token': token}

        message = pubsub['serial'].get_message()['data']
        assert pubsub['serial'].get_message() is None

        payload = json.loads(message)
        assert payload == {'udid': plist['UDID'], 'serial_number': plist['SERIAL']}
        assert new_device.id == device.id

    def test_response_200_from_mdm_enroll(self, mdm_app, mdm_profile):
        config = MDMConfig(**mdm_profile)
        actions.PushCertificate.get_topic = MagicMock(return_value='topic')
        cdatabase.db_session.add(config)
        cdatabase.db_session.commit()
        actions.db_session = MagicMock()
        query = MagicMock()
        actions.db_session.query.return_value = query
        query.first.return_value = config
        mdm_client = mdm_app.test_client(use_cookies=True)
        res = mdm_client.get(url_for('mdm_app.enroll', id_token='F6631968-54D7-45BD-8744-704CFC1D0C50'))
        assert res.status_code == 200

        created_config = cdatabase.db_session.query(MDMConfig).first()
        assert created_config.topic == mdm_profile['topic'] 

    def test_response_500_from_mdm_enroll_when_wrong_device_identity_method(self, mdm_app, mdm_profile):
        config = MDMConfig(**mdm_profile)
        config.device_identity_method = 'scep'
        cdatabase.db_session.add(config)
        cdatabase.db_session.commit()
        actions.db_session = MagicMock()
        query = MagicMock()
        actions.db_session.query.return_value = query
        query.first.return_value = config

        actions.PushCertificate.get_topic = MagicMock(return_value='topic')
        mdm_client = mdm_app.test_client(use_cookies=True)
        res = mdm_client.get(url_for('mdm_app.enroll', id_token='F6631968-54D7-45BD-8744-704CFC1D0C50'))
        assert res.status_code == 500
