import pytest
from fixtures import mdm_app, device as device_fixture, auth_plist_data, token_plist_data, exist_device, \
    certificate as certificate_fixture
from mock import MagicMock, Mock
from flask import url_for, g
from commandment.mdm import device
from commandment.mdm import actions
from commandment import database as cdatabase
from commandment.models import Certificate, Device


class TestCheckin:
    def test_do_checkin_with_authenticate_message(self, mdm_app, device_fixture, auth_plist_data, certificate_fixture):
        device_inst = Device(**device_fixture)
        cert = Certificate(**certificate_fixture)
        cdatabase.db_session.add(cert)
        cdatabase.db_session.commit()
        device_inst.certificate = cert
        cdatabase.db_session.add(device_inst)
        cdatabase.db_session.add(cert)
        cdatabase.db_session.commit()

        g.device_cert = cert
        g.plist_data = auth_plist_data
        actions.db_session = cdatabase.db_session
        result = actions.do_checkin(id_token='F6631968-54D7-45BD-8744-704CFC1D0C50')
        assert result == 'OK'

        created_device = cdatabase.db_session.query(Device).filter(Device.udid == auth_plist_data['UDID']).one()
        assert created_device.id == 2

    def test_do_checkin_with_token_update_message(self, mdm_app, exist_device, token_plist_data):
        device_inst = Device(**exist_device)
        cdatabase.db_session.add(device_inst)
        cdatabase.db_session.commit()
        g.device_cert = device_inst.certificate
        g.plist_data = token_plist_data
        g.device = device_inst
        actions.db_session = cdatabase.db_session
        query = MagicMock()
        query.first.return_value = device_inst
        result = actions.do_checkin(id_token='F6631968-54D7-45BD-8744-704CFC1D0C50')
        assert result == 'OK'

        updated_device = cdatabase.db_session.query(Device).filter(Device.udid == token_plist_data['UDID']).one()
        assert updated_device.topic == token_plist_data['Topic']
