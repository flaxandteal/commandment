import os
from setuptools import setup

setup(
    name='commandment',
    scripts=[
        'scripts/commandment_worker',
    ],
    packages=[
        'commandment',
        'commandment.mdmcmds',
        'commandment.scep',
        'commandment.pki',
        'commandment.orm',
        'commandment.utils',
        'commandment.profiles',
        'commandment.mdm'
    ],
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
    package_data={'config': ['config/config.json']}
)
