#!/usr/bin/env python
'''
Copyright (c) 2015 Jesse Peterson
Licensed under the MIT license. See the included LICENSE.txt file for details.
'''

import os
import json
import pkg_resources
from commandment import default_settings

class ConfigurationBuilder:
    _configuration = None

    MODE_WEB = 0
    MODE_API = 1

    def __init__(self):
        self._configuration = {
            'debug': False
        }

    def load(self):
        try:
            configuration_location = pkg_resources.Requirement.parse('commandment')
            configuration_file = pkg_resources.resource_filename(
                configuration_location,
                'config/config.json'
            )
        except:
            configuration_file = 'config/config.json'

        if os.path.exists(configuration_file):
            with open(configuration_file, 'r') as configuration_fh:
                loaded_configuration = json.load(configuration_fh)
            self._configuration.update(loaded_configuration)

    def parse(self):
        configuration = self._configuration
        for key in ('host', 'port', 'pass', 'database'):
            keyu = key.upper()
            if os.environ.get('REDIS_%s' % keyu):
                configuration['redis'][key] = os.environ.get('REDIS_%s' % keyu)

        if os.environ.get('COMMANDMENT_DEBUG') == '1':
            configuration['debug'] = True

        if 'database' not in configuration:
            configuration['database'] = {
                'uri': default_settings.DATABASE_URI,
                'echo': default_settings.DATABASE_ECHO
            }

        database_envs = [
            'DATABASE_DIALECT',
            'DATABASE_USER',
            'DATABASE_PASSWORD',
            'DATABASE_HOST',
            'DATABASE_PORT',
            'DATABASE_NAME'
        ]
        database_env_map = {k: os.environ.get(k) for k in database_envs}
        if None not in database_env_map.values():
            configuration['database']['uri'] = "{DATABASE_DIALECT}://{DATABASE_USER}:{DATABASE_PASSWORD}@{DATABASE_HOST}:{DATABASE_PORT}/{DATABASE_NAME}".format(**database_env_map)

        if os.environ.get('DATABASE_URI'):
            configuration['database']['uri'] = os.environ.get('DATABASE_URI')

        if os.environ.get('COMMANDMENT_MODE_API'):
            configuration['mode'] = self.MODE_API
        else:
            configuration['mode'] = self.MODE_WEB

        if os.environ.get('PUSH_SUPPRESS') == '1':
            configuration['PUSH_SUPPRESS'] = True
        else:
            configuration['PUSH_SUPPRESS'] = False

        if os.environ.get('PUSH_GATEWAY'):
            configuration['PUSH_GATEWAY'] = os.environ.get('PUSH_GATEWAY')
        else:
            configuration['PUSH_GATEWAY'] = None

    def get(self):
        return self._configuration

builder = ConfigurationBuilder()
