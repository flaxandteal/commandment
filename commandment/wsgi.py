#!/usr/bin/env python
'''
Copyright (c) 2015 Jesse Peterson
Licensed under the MIT license. See the included LICENSE.txt file for details.
'''

import time
from flask_redis import FlaskRedis
from commandment.database import config_engine, init_db, db_session
from commandment.pki.ca import get_or_generate_web_certificate,\
    reset_web_certificates
from commandment.push import push_init
from commandment.pki import x509
from commandment.mdm.actions import build_mdm_config
from commandment import models, configuration as config

def setup():
    config_builder = config.ConfigurationBuilder()
    config_builder.load()
    config_builder.parse()
    configuration = config_builder.get()

    if configuration['mode'] == config.ConfigurationBuilder.MODE_API:
        from commandment.api_app import create_api_app
        app = create_api_app(configuration['debug'], FlaskRedis(), configuration)
    else:
        from commandment.app import create_app
        app = create_app(configuration['debug'], FlaskRedis(), configuration)

    app.logger.info(configuration)

    app.config.update(configuration)

    config_engine(configuration['database']['uri'], configuration['database']['echo'])

    init_db()

    app.logger.info("Waiting for worker to set up DB")

    pubsub = app.redis_store.pubsub()
    pubsub.subscribe('commandment.init.complete')
    if app.redis_store.get('commandment.init.completed') == 'TRUE':
        app.logger.info("init completed before we waited")
    else:
        for message in pubsub.listen():
            if message and message['data'] == 'TRUE':
                break
            app.logger.info("... waiting for init completion ...")

    app.logger.info("Got init completion event, continuing")

    push_init()

    return app, configuration['debug']

app, debug = setup()
if __name__ == '__main__':
    app.run(threaded=False, use_reloader=False, debug=debug)
