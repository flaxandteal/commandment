from flask import current_app, abort
from ..mdmcmds import InstallProfile, RemoveProfile
from ..models import ProfileStatus
from ..database import db_session

class ProfileService:
    def install(self, profile, old_profiles=None):
        if profile.status == ProfileStatus.ACTIVE:
            return {"message": "Already active"}

        if profile.status != ProfileStatus.INACTIVE:
            abort(400, "Profile must be inactive to activate" + str(profile.status))

        if old_profiles:
            for old_profile in old_profiles:
                if old_profile.status == ProfileStatus.PENDING_INSTALLATION:
                    db_session.delete(old_profile)
            db_session.commit()

        profile.status = ProfileStatus.PENDING_INSTALLATION

        current_app.redis_queue.enqueue(
            'commandment.tasks.process_profile_deployment_change',
            profile.id
        )

    def remove(self, profile, delete=False, old_profiles=None):
        if profile.status == ProfileStatus.INACTIVE:
            return {"message": "Already deactivated"}

        if profile.status != ProfileStatus.ACTIVE:
            abort(400, "Profile must be active to deactivate" + str(profile.status))

        deployment_change = False

        if delete:
            if old_profiles:
                for old_profile in old_profiles:
                    if old_profile.status == ProfileStatus.PENDING_DELETION:
                        db_session.delete(old_profile)
                db_session.commit()

            profile.status = ProfileStatus.PENDING_DELETION

            current_app.logger.warning('Added device to group')

            deployment_change = True
        else:
            profile.status = ProfileStatus.PENDING_REMOVAL

            # In this case, this profile is already being removed from devices
            if old_profiles:
                deployment_change = not any([p.status == ProfileStatus.PENDING_REMOVAL for p in old_profiles])
            else:
                deployment_change = True

        db_session.commit()

        if deployment_change:
            current_app.redis_queue.enqueue('commandment.tasks.process_profile_deployment_change', profile.id)

    def _finalize_command(self, device, command, input_data):
        new_qc = command.new_queued_command(device, input_data)
        db_session.add(new_qc)

    def finalize_removal(self, profile, device):
        input_data = {'Identifier': profile.identifier, 'UUID': profile.uuid}
        self._finalize_command(
            device,
            RemoveProfile,
            input_data
        )

    def finalize_installation(self, profile, device):
        input_data = {'id': profile.id}

        self._finalize_command(
            device,
            InstallProfile,
            input_data
        )
