# coMmanDMent Open Source MDM

Commandment is an Open Source Apple MDM server with support for managing iOS and macOS devices implemented in Python. The source code is available under an [MIT license](LICENSE.txt).

This is a stripped down version of [jessepeterson/commandment](https://github.com/jessepeterson/commandment) providing a REST API, allowing the building of third party frontends, apps, etc.
Patches will be prepared and submitted upstream.

For more information, see the [upstream README](README-upstream.md).

## Containers

Starting the containerized version of Commandment:
* `sudo make run-db` (first time only - leave for a few seconds until database built)
* `sudo make run`

Stopping containerized version:
* `sudo make stop`
